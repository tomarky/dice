オープンアプリ『サイコロ』

ボードゲームのサイコロを失くしてしまったときなどに
このアプリでサイコロの代替ができます


アプリ配布リンク: http://tomarky.html.xdomain.jp/mobile/oap/p030.hdml


※オープンアプリとはau携帯電話(ガラケー)で動かすことができるアプリのことです
　(参考リンク http://www.au.kddi.com/ezfactory/tec/spec/openappli.html ）