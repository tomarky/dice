// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyMenu.java  クラス  version 1.0
//
// (C)Tomarky   201*/01/01 - 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //乱数や時間
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * メニュークラス
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class MyMenu implements OAPConstants, DrawConstants {
	

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	/** メニューリスト */
	private Vector   menulist=null;

	/** メニューの選択の可不可(視覚的表現のみ) */
	private Vector   menuselectable=null;

	/** タイトル */
	private String   title=null;
	
	/** 最大表示数 */
	private int      maxview=9;

	/** メニューの幅 */
	private int      menuwidth=0;
	
	/** 各メニューの高さ */
	private int      menuheight=0;
	
	/** メニューの数 */
	private int      menucount=0;

	/** 選択されてるメニューインデックス */
	private int      selected=0;
	
	/** 一番上に表示されるメニューのインデックス */
	private int      topindex=0;


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/** 
	 * コンストラクタ
	 *
	 * @param wordlength 表示する最大文字数(全角)
	 */
	public MyMenu(int wordlength) {
		menulist=new Vector(10);
		menuselectable=new Vector(10);
		menuwidth=FONTSIZE_SMALL*wordlength+4;
		menuheight=FONTSIZE_SMALL+4;
	}


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@ //
	// @@@@ private @@@@ //
	// @@@@@@@@@@@@@@@@@ //
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 有効なインデックスか確認する。
	 *
	 * @param  index 確認したいインデックス番号。
	 * @return 有効ならtrue,無効ならfalseを返す。
	 */
	private boolean checkIndex(int index) {
		return (0<=index)&&(index<menucount);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// @@@@@@@@@@@@@@@@ //
	// @@@@ public @@@@ //
	// @@@@@@@@@@@@@@@@ //
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * メニュータイトルを設定する。
	 * 
	 * @param newtitle メニュータイトル。
	 */
	public void setTitle(String newtitle) {
		title=new String(newtitle);
	}
	
	public String getTitle() {
		return title;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * メニューのインデックスの取得。
	 *
	 * @return メニューのインデックスを返す。存在しない場合-1を返す。
	 */
	public int indexOf(String findmenu) {
		return menulist.indexOf(findmenu);
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * メニューを追加する。
	 * 
	 * @param newmenu 追加するメニュー。
	 */
	public void addMenu(String newmenu) {
		menulist.addElement(new String(newmenu));
		menuselectable.addElement(new Boolean(true));
		menucount++;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * 指定位置にメニューを追加する。
	 * 無効なインデックスが指定された場合何もしない。
	 * 
	 * @param newmenu 追加するメニュー。
	 * @param index   追加する位置のインデックス。
	 */
	public void insertMenu(String newmenu, int index) {
		if (!checkIndex(index)) return;
		menulist.insertElementAt(newmenu,index);
		menuselectable.insertElementAt(new Boolean(true),index);
		menucount++;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	/** 
	 * 指定インデックスのメニューを変更する。
	 * 無効なインデックスが指定された場合何もしない。
	 * 
	 * @param newmenu 新しいメニュー。
	 * @param index   変更するメニューのインデックス。
	 */
	public void setMenu(String newmenu, int index) {
		if (!checkIndex(index)) return;
		menulist.removeElementAt(index);
		menulist.insertElementAt(newmenu,index);
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	/**
	 * メニューを削除する。
	 * 無効なインデックスが指定された場合何もしない。
	 *
	 * @param index 削除するメニューのインデックス。
	 */
	public void removeMenu(int index) {
		if (!checkIndex(index)) return;
		menulist.removeElementAt(index);
		menuselectable.removeElementAt(index);
		menucount--;
		if (selected>=menucount) {
			selected=menucount-1;
		}
		if (topindex+maxview>=menucount) {
			if (topindex>0) {
				topindex--;
			}
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void removeMenuAll() {
		menulist.removeAllElements();
		menuselectable.removeAllElements();
		menucount=0;
		selected=0;
		topindex=0;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	/**
	 * メニューを取得。
	 *
	 * @param index メニューのインデックスを渡す。
	 * @return メニュー名のコピーを返す。不正なインデックスの場合はnullを返す。
	 */
	public String getMenu(int index) {
		if (!checkIndex(index)) return null;
		return new String((String)menulist.elementAt(index));
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 選択可不可の設定。
	 *
	 * @param index インデックス。
	 */
	public void setSelectable(boolean selectable, int index) {
		if (!checkIndex(index)) return;
		menuselectable.removeElementAt(index);
		menuselectable.insertElementAt(new Boolean(selectable),index);
	}

/*
	public void setSelectable(boolean selectable, String menu) {
		int index=menulist.indexOf(menu);
		if (index<0) return;
		menuselectable.removeElementAt(index);
		menuselectable.insertElementAt(new Boolean(selectable),index);
	}
*/

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 選択可不可の取得。
	 *
	 * @param index インデックス。
	 * @return 可不可。
	 */
	public boolean getSelectable(int index) {
		if (!checkIndex(index)) return false;
		return ((Boolean)(menuselectable.elementAt(index))).booleanValue();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * 選択されているメニューのインデックスの取得。
	 * 
	 * @return 選択されたメニューインデックスを返す。
	 */
	public int getSelect() {
	 	return selected;
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 選択されているメニュー名の取得。
	 *
	 * @return メニュー名のコピーを返す。メニューが無い場合nullを返す。
	 */
	public String getSelectMenu() {
		if (menucount>0) {
			return new String((String)menulist.elementAt(selected));
		} else {
			return null;
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * メニュータイトルを設定する。
	 * 無効なインデックスが指定された場合何もしない。
	 * 
	 * @param index 選択状態にするメニューインデックス
	 */
	public void setSelect(int index) {
		if (!checkIndex(index)) return;
		selected=index;
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * ひとつ上にカーソルを移動する。
	 * カレントカーソルが一番上なら一番下に移動する。
	 */ 
	public void up() {
		if (menucount==0) return;
		selected=(selected+menucount-1)%menucount;
		if (selected<topindex) {
			topindex=selected;
		} else if (selected==menucount-1) {
			if (selected>maxview-1) {
				topindex=selected-(maxview-1);
			}
		}
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * ひとつ下に選択カーソルを移動する。
	 * カレントカーソルが一番下なら一番上に移動する。
	 */
	public void down() {
		if (menucount==0) return;
		selected=(selected+1)%menucount;
		if (selected>topindex+maxview-1) {
			topindex=selected-(maxview-1);
		} else {
			if (selected==0) {
				topindex=0;
			}
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * メニュータイトルを設定する。
	 * 
	 * @param newtitle メニュータイトル
	 */
	public void paint(Graphics g) {
		int dx=(DISPLAY_WIDTH-menuwidth)/2;
		int dy;
		int cx=DISPLAY_WIDTH/2;
		int count=menucount;

		if (menucount>maxview) {
			dy=(DISPLAY_HEIGHT-menuheight*10)/2;
			count=maxview;
		} else {
			dy=(DISPLAY_HEIGHT-menuheight*(count+1))/2;
		}
		
		g.setFont(FONT_SMALL);
		
		g.setColor(COLOR_DARKCYAN); //DarkCyan
		g.fillRect(dx,dy,menuwidth,menuheight);
		g.setColor(COLOR_BLACK); //Black
		g.drawString(title+(menucount>maxview?(" "+Integer.toString(selected+1)
		                                          +"/"+Integer.toString(menucount)):"")
		                  ,cx,dy+2,ANCHOR_CT);
		
		for (int i=0;i<count;i++) {
			g.setColor((topindex+i==selected)?COLOR_YELLOW:COLOR_GRAY);
			g.fillRect(dx,dy+menuheight*(i+1),menuwidth,menuheight);
			if (((Boolean)(menuselectable.elementAt(topindex+i))).booleanValue()) {
				g.setColor((topindex+i==selected)?COLOR_RED:COLOR_BLACK);
			} else {
				g.setColor((topindex+i==selected)?COLOR_GRAY:COLOR_SILVER);
			}
			g.drawString((String)menulist.elementAt(topindex+i),
			                cx,dy+menuheight*(i+1)+2,ANCHOR_CT);
		}

		g.setColor(COLOR_WHITE); //White
		for (int i=0;i<=count;i++) {
			g.drawRect(dx,dy+menuheight*i,menuwidth,menuheight);
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //MyMenuの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
