// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// SpDiceMode.java  クラス  version 1.0
//
// (C)Tomarky   201*/01/01 - 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オリジナルクラス
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class SpDiceMode extends Mode implements DataSetter {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	private MIDlet        midlet=null;
	private Displayable   bDisp=null;

	private MyMenu        currentmenu=null;

	private int           editmode=0;
	private int           seldice=-1;

	private int[]         dice_type=new int[10];
	private int[]         dice_ID=new int[10];
	private String[]      dice_name=new String[10];
	private Hashtable     valuelist=new Hashtable(10);

	private MyForm        form=null;
	private MyList        list=null;
	
	private Refresher     refresher=null;

	private int           diceNextID=0;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/** 
	 * コンストラクタ
	 */
	public SpDiceMode(MIDlet midlet, Refresher rf) {
		randomlist=new MyRandom[10];
		dice_number=new int[10];
		this.midlet=midlet;
		refresher=rf;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public String name() {
		return "特殊サイコロ";
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int keyMenu(int keyEvent) {
		if (currentmenu==null) {
			return NOCHANGE;
		}
		int n=currentmenu.getSelect();
		
		if (editmode==0) {
			switch (n) {
			case 0:
				currentmenu.setSelect(0);
				editmode=1;
				break;
			case 1:
				if (dice_count>0) {
					totalshown=!totalshown;
				}
				break;
			case 2:
				animation=!animation;
				break;
			case 3:
				if (animation) {
					autostop=!autostop;
				}
				break;
			case 4:
				if (animation) {
					stopall=!stopall;
				}
				break;
			case 5:
				switch (keyEvent) {
				case Canvas.LEFT:
					return BACK;
				case Canvas.RIGHT:
				case Canvas.FIRE:
					return NEXT;
				}
				break;
			}

		} else {
			switch (n) {
			case 0: //追加
				if (dice_count<10) {
					if (form==null) {
						form=new MyForm(this,midlet);
					} else {
						form.reset();
					}
					Display d=Display.getDisplay(midlet);
					bDisp=d.getCurrent();
					d.setCurrent(form);
				}
				break;
			case 1: //変更
				if (dice_count>0) {
					editmode=5;
				}
				break;
			case 2: //コピー
				if ((dice_count>0)&&(dice_count<10)) {
					editmode=2;
				}
				break;
			case 3: //削除
				if (dice_count>0) {
					editmode=3;
				}
				break;
			case 4: //セーブ
				if (dice_count>0) {
					editmode=4;
				}
				break;
			case 5: //ロード
				if (dice_count<10) {
					if (list==null) {
						list=new MyList(this);
					}
					list.reset();
					Display d=Display.getDisplay(midlet);
					bDisp=d.getCurrent();
					d.setCurrent(list);
				}
				break;
			case 6:
				currentmenu.setSelect(0);
				editmode=0;
				break;
			}
		}
		
		return NOCHANGE;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public boolean keyPressed(int keyEvent) {
		if (editmode==0) {
			return super.keyPressed(keyEvent);
		} else {
			if (dice_count>0) {
				switch (keyEvent) {
				case Canvas.UP:
					seldice=(seldice+dice_count-1)%dice_count;
					break;
				case Canvas.DOWN:
					seldice=(seldice+1)%dice_count;
					break;
				case Canvas.FIRE:
					switch (editmode) {
					case 2:
						editmode=1;
						copyDice();
						break;
					case 3:
						editmode=1;
						deleteDice();
						break;
					case 4:
						if (saveDice()) {
							editmode=41;
						} else {
							editmode=42;
						}
						break;
					case 5: //変更
						editDice();
						break;
					case 41:
					case 42:
						editmode=1;
						break;
					}
					break;
				case KEY_CLEAR:
					switch (editmode) {
					case 2:
					case 3:
					case 4:
					case 5:
					case 41:
					case 42:
						editmode=1;
						break;
					}
					break;
				}
			}
		}
		return false;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void paint(Graphics g) {
		
		if (dice_count==0) {
			g.setColor(COLOR_WHITE);
			g.setFont(FONT_SMALL);
			g.drawString("サイコロがありません",DISPLAY_WIDTH/2,DISPLAY_HEIGHT/2,ANCHOR_CT);
			if (editmode==1) {
				g.drawString("メニューから作業を",DISPLAY_WIDTH/2,200,ANCHOR_CT);
				g.drawString("選んでください",DISPLAY_WIDTH/2,214,ANCHOR_CT);
			}
			return;
		}
		
		if ((seldice>=0)&&(editmode>0)) {
			g.setColor(COLOR_YELLOW);
			g.drawRect(0,20+17*seldice,DISPLAY_WIDTH-1,17);
		}
		
		int[] ilist=null;
		String[] slist=null;
		for (int i=0;i<dice_count;i++) {
			g.setFont(FONT_SMALL);
			g.setColor(COLOR_WHITE);
			if (rolling) {
				if (!stopall) {
					if (i==dice_decision) {
						g.setColor(autostop?COLOR_MAGENTA:COLOR_CYAN);
					}
				}
			}
			g.drawString(dice_name[i],0,20+17*i,ANCHOR_LT);
			g.setFont(FONT_MEDIUM);
			if (rolling) {
				if (stopall) {
					g.setColor(autostop?COLOR_MAGENTA:COLOR_CYAN);
				} else {
					if (i==dice_decision) {
						g.setColor(autostop?COLOR_MAGENTA:COLOR_CYAN);
					} else if (i<dice_decision) {
						g.setColor(COLOR_WHITE);
					} else {
						g.setColor(COLOR_YELLOW);
					}
				}
			} else {
				g.setColor(COLOR_WHITE);
			}
			switch (dice_type[i]) {
			case 0:
			case 1:
				g.drawString(Integer.toString(dice_number[i]),
				              DISPLAY_WIDTH,20+17*i,ANCHOR_RT);
				break;
			case 2:
				ilist=(int[])(valuelist.get(new Integer(dice_ID[i])));
				g.drawString(Integer.toString(ilist[dice_number[i]]),
				              DISPLAY_WIDTH,20+17*i,ANCHOR_RT);
				break;
			case 3:
				slist=(String[])(valuelist.get(new Integer(dice_ID[i])));
				g.drawString(slist[dice_number[i]],
				              DISPLAY_WIDTH,20+17*i,ANCHOR_RT);
				break;
			}
		}
		
		g.setFont(FONT_SMALL);
		g.setColor(COLOR_WHITE);
		switch (editmode) {
		case 0:
			super.paint(g);
			break;
		case 1:
			g.drawString("メニューから作業を",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 2:
			g.drawString("コピーするサイコロを",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 3:
			g.drawString("削除するサイコロを",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 4:
			g.drawString("セーブするサイコロを",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 5:
			g.drawString("変更するサイコロを",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 41:
			g.drawString("セーブしました",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		case 42:
			g.drawString("セーブに失敗しました",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			break;
		}
		if ((editmode>0)&&(editmode<6)) {
			g.drawString("選んでください",DISPLAY_WIDTH/2,214,ANCHOR_CT);
		}

	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public MyMenu getMenu(boolean modechange) {
		if (rolling) {
			return null;
		}
		if (1<editmode) {
			return null;
		}
		MyMenu menu=new MyMenu(10);
		if (editmode==0) {
			menu.setTitle("メニュー");
			menu.addMenu("サイコロ編集 "+Integer.toString(dice_count)+"/10");
			menu.addMenu("合計表示 "+(totalshown?"ON":"OFF"));
			menu.addMenu("ｱﾆﾒｰｼｮﾝ "+(animation?"ON":"OFF"));
			menu.addMenu("自動停止 "+(autostop?"ON":"OFF"));
			menu.addMenu("一斉停止 "+(stopall?"ON":"OFF"));
			menu.addMenu("モード変更");
			if (dice_count==0) {
				menu.setSelectable(false,1);
			}
			if (animation==false) {
				menu.setSelectable(false,3);
				menu.setSelectable(false,4);
			}
			if (modechange) {
				menu.setSelect(5);
			}
		} else {
			menu.setTitle("サイコロ編集 "+Integer.toString(dice_count)+"/10");
			menu.addMenu("追加");
			menu.addMenu("変更");
			menu.addMenu("コピー");
			menu.addMenu("削除");
			menu.addMenu("セーブ");
			menu.addMenu("ロード");
			menu.addMenu("編集終了");
			if (dice_count==10) {
				menu.setSelectable(false,0);
				menu.setSelectable(false,2);
				menu.setSelectable(false,5);
			}
			if ((dice_count==0)||(seldice<0)) {
				menu.setSelectable(false,1);
				menu.setSelectable(false,2);
				menu.setSelectable(false,3);
				menu.setSelectable(false,4);
			}
		}

		if (currentmenu!=null) {
			int n=currentmenu.getSelect();
			menu.setSelect(n);
			currentmenu.removeMenuAll();
		}
		
		currentmenu=menu;
		menu=null;
		return currentmenu;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void setData(String name, int type, int n1, int n2, int n3, int[] ilist, String[] slist) {
		if (name!=null) {
			int n=0,newID=0;
			if (editmode==5) {
				editmode=1;
				n=seldice;
				newID=dice_ID[n];
			} else {
				n=dice_count;
				dice_count++;
				newID=dice_ID[n]=diceNextID;
				diceNextID++;
			}
			switch (type) {
			case 0:
				randomlist[n]=new MyRandom(n1,n2);
				break;
			case 1:
				randomlist[n]=new MyRandom(n1,n2,n3+1);
				break;
			case 2:
				valuelist.put(new Integer(newID),ilist);
				randomlist[n]=new MyRandom(0,ilist.length-1);
				break;
			case 3:
				valuelist.put(new Integer(newID),slist);
				randomlist[n]=new MyRandom(0,slist.length-1);
				break;
			}
			dice_number[n]=randomlist[n].getNext();
			dice_type[n]=type;
			dice_name[n]=name;
			if (seldice<0) {
				seldice=0;
				currentmenu.setSelectable(true,1);
				currentmenu.setSelectable(true,2);
				currentmenu.setSelectable(true,3);
				currentmenu.setSelectable(true,4);
			}
			if (dice_count==10) {
				currentmenu.setSelectable(false,0);
				currentmenu.setSelectable(false,2);
				currentmenu.setSelectable(false,5);
			}
			if (dice_count>2) {
				calcDiceTotal();
			}
			currentmenu.setTitle("サイコロ編集 "+Integer.toString(dice_count)+"/10");
			refresher.refresh();
		} else {
			if (editmode==5) {
				editmode=1;
				refresher.refresh();
			}
		}
		(Display.getDisplay(midlet)).setCurrent(bDisp);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void copyDice() {
		int n=seldice,p;
		switch (dice_type[n]) {
		case 0:
			setData(dice_name[n],0,
			          randomlist[n].min(),
			          randomlist[n].max(),
			          0,null,null);
			break;
		case 1:
			if (randomlist[n].increase()<0) {
				p=randomlist[n].max();
			} else {
				p=randomlist[n].min();
			}
			setData(dice_name[n],1,
			          p,randomlist[n].increase(),
			          randomlist[n].count(),null,null);
			break;
		case 2:
			int[] ilist1=(int[])valuelist.get(new Integer(dice_ID[n]));
			int[] ilist2=new int[ilist1.length];
			System.arraycopy(ilist1,0,ilist2,0,ilist1.length);
			setData(dice_name[n],2,0,0,0,ilist2,null);
			break;
		case 3:
			String[] slist1=(String[])valuelist.get(new Integer(dice_ID[n]));
			String[] slist2=new String[slist1.length];
			System.arraycopy(slist1,0,slist2,0,slist1.length);
			setData(dice_name[n],3,0,0,0,null,slist2);
			break;
			
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void deleteDice() {
		int n=seldice;
		switch (dice_type[n]) {
		case 2:
		case 3:
			valuelist.remove(new Integer(dice_ID[n]));
			break;
		}
		for (int i=n;i<dice_count-1;i++) {
			randomlist[i]=randomlist[i+1];
			dice_number[i]=dice_number[i+1];
			dice_name[i]=dice_name[i+1];
			dice_type[i]=dice_type[i+1];
			dice_ID[i]=dice_ID[i+1];
		}
		dice_count--;
		if (seldice>=dice_count) {
			seldice=dice_count-1;
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private boolean saveDice() {
		boolean rslt=false;
		int n=seldice;
		switch (dice_type[n]) {
		case 0:
			rslt=MyList.save(dice_name[n],0,
			                randomlist[n].min(),
			                randomlist[n].max(),
			                0,null,null);
			break;
		case 1:
			int p=0;
			if (randomlist[n].increase()<0) {
				p=randomlist[n].max();
			} else {
				p=randomlist[n].min();
			}
			rslt=MyList.save(dice_name[n],1,
			                   p,randomlist[n].increase(),
			                   randomlist[n].count(),null,null);
			break;
		case 2:
			int[] ilist1=(int[])valuelist.get(new Integer(dice_ID[n]));
			rslt=MyList.save(dice_name[n],2,0,0,0,ilist1,null);
			break;
		case 3:
			String[] slist1=(String[])valuelist.get(new Integer(dice_ID[n]));
			rslt=MyList.save(dice_name[n],3,0,0,0,null,slist1);
			break;
		}
		return rslt;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void editDice() {
		if (form==null) {
			form=new MyForm(this,midlet);
		}
		int n=seldice;
		switch (dice_type[n]) {
		case 0:
			form.reset(dice_name[n],0,
			                randomlist[n].min(),
			                randomlist[n].max(),
			                0,null,null);
			break;
		case 1:
			int p=0;
			if (randomlist[n].increase()<0) {
				p=randomlist[n].max();
			} else {
				p=randomlist[n].min();
			}
			form.reset(dice_name[n],1,
			                   p,randomlist[n].increase(),
			                   randomlist[n].count(),null,null);
			break;
		case 2:
			int[] ilist1=(int[])valuelist.get(new Integer(dice_ID[n]));
			form.reset(dice_name[n],2,0,0,0,ilist1,null);
			break;
		case 3:
			String[] slist1=(String[])valuelist.get(new Integer(dice_ID[n]));
			form.reset(dice_name[n],3,0,0,0,null,slist1);
			break;
		}
		Display d=Display.getDisplay(midlet);
		bDisp=d.getCurrent();
		d.setCurrent(form);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 *
	 */
	protected void calcDiceTotal() {
		int[] ilist=null;
		dice_total=0;
		for (int i=0;i<dice_count;i++) {
			switch (dice_type[i]) {
			case 0:
			case 1:
				dice_total+=dice_number[i];
				break;
			case 2:
				ilist=(int[])(valuelist.get(new Integer(dice_ID[i])));
				dice_total+=ilist[dice_number[i]];
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //SpDiceModeの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
