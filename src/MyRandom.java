// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRandom.java  ランダムリストクラス  version 1.0
//
// (C)Tomarky   201*/01/01 - 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
//import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * ランダムリストクラス。
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class MyRandom {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//
	
	/** ランダムジェネレータ。 */
	public static final Random RANDOM=new Random();
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //
	
	/** シャッフル回数。 */
	private static int  shuffle_count=5;

	/** ランダムリスト。 */
	private int[]       randomlist=null;
	
	/** インデックス。 */
	private int         index=0;

	private int         minNumber=0;
	private int         maxNumber=0;
	private int         numInc=0;
	private int         numCount=0;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/** 
	 * minからmaxまでのランダムリスト。
	 *
	 * @param min 最小。
	 * @param max 最大。
	 */
	public MyRandom(int min, int max) {
		minNumber=min;
		maxNumber=max;
		if (!(minNumber<maxNumber)) {
			int t=maxNumber;
			maxNumber=minNumber;
			minNumber=t;
		}
		numInc=1;
		numCount=maxNumber-minNumber+1;
		if (numCount<=20) {
			randomlist=new int[maxNumber-minNumber+1];
			for (int i=0; i<randomlist.length; i++) {
				randomlist[i]=i+minNumber;
			}
			shuffle();
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * startから始まりincずつ増加する個数countのランダムリスト。
	 *
	 * @param start スタート。
	 * @param inc   増加量。
	 * @param count 個数。
	 */
	public MyRandom(int start, int inc, int count) {
		if (count<1) {
			count=1;
		}
		if (inc<0) {
			minNumber=inc*(count-1)+start;
			maxNumber=start;
		} else {
			minNumber=start;
			maxNumber=inc*(count-1)+start;
		}
		numInc=inc;
		numCount=count;
		if (count<=20) {
			randomlist=new int[count];
			for (int i=0; i<randomlist.length; i++) {
				randomlist[i]=start+i*inc;
			}
			shuffle();
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/** 
	 * 数字リストから生成するランダムリスト。
	 *
	 * @param list 数字リスト。
	 */
	public MyRandom(int[] list) {
		if (list!=null) {
			if (list.length>0) {
				randomlist=new int[list.length];
				System.arraycopy(list,0,randomlist,0,list.length);
			} else {
				randomlist=new int[1];
				randomlist[0]=0;
			}
		} else {
			randomlist=new int[1];
			randomlist[0]=0;
		}
		minNumber=maxNumber=randomlist[0];
		for (int i=1;i<randomlist.length;i++) {
			if (randomlist[i]<minNumber) {
				minNumber=randomlist[i];
			}
			if (randomlist[i]>maxNumber) {
				maxNumber=randomlist[i];
			}
		}
		numInc=0;
		numCount=randomlist.length;
		shuffle();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * ランダムリストを複製。
	 *
	 * @param myrandom ランダムリスト。
	 */
	public MyRandom(MyRandom myrandom) {
		if (myrandom.randomlist!=null) {
			randomlist=new int[(myrandom.randomlist).length];
			System.arraycopy(myrandom.randomlist,0,
			                  randomlist,0,randomlist.length);
		}
		index=myrandom.index;
		maxNumber=myrandom.maxNumber;
		minNumber=myrandom.minNumber;
		numInc=myrandom.numInc;
		numCount=myrandom.numCount;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * シャッフル
	 */
	public void shuffle() {
		if (randomlist==null) {
			return;
		}
		int v=0,p=0,c=0,i,j;
		c=randomlist.length;
		for (j=0;j<shuffle_count;j++) {
			for (i=0;i<c;i++) {
				p=MyRandom.rand(c);
				v=randomlist[i];
				randomlist[i]=randomlist[p];
				randomlist[p]=v;
			}
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 次の数
	 *
	 * @return 数
	 */
	public int getNext(boolean doshuffle) {
		if (randomlist==null) {
			int n=MyRandom.rand(numCount);
			return minNumber+Math.abs(numInc)*n;
		} else {
			if (doshuffle) {
				if (index==0) {
					shuffle();
				}
			}
			index=(index+1)%randomlist.length;
			return randomlist[index];
		}
	} 

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 次の数
	 *
	 * @return 数
	 */
	public int getNext() {
		if (randomlist==null) {
			int n=MyRandom.rand(numCount);
			return minNumber+Math.abs(numInc)*n;
		} else {
			if (index==0) {
				shuffle();
			}
			index=(index+1)%randomlist.length;
			return randomlist[index];
		}
	} 

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int count() {
		return numCount;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int max() {
		return maxNumber;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int min() {
		return minNumber;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int increase() {
		return numInc;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 乱数生成
	 *
	 * @param num 生成する乱数の大きさ
	 * @return    生成した乱数を返す
	 */
	public static int rand(int num) {
		return ((MyRandom.RANDOM).nextInt()>>>1)%num;
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //MyRandomの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
