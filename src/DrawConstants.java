// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// DrawConstants.java  描写処理を便利にするための定数群   version 1.0
//
// (C)Tomarky   201*/01/01 -
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 
// javadoc作成時
// 置換 "///" → ""

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インターフェースのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * 描写処理を便利にするための定数群
 *
 * @author  Tomarky
 * @version 1.0
 */
public interface DrawConstants {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@ フォント @@@@ //

	/** Planeスタイル Smallサイズ */
	Font FONT_SMALL     = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL);
	/** Planeスタイル Mediumサイズ */
	Font FONT_MEDIUM    = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM);
	/** Planeスタイル Largeサイズ */
	Font FONT_LARGE     = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE);
	
///	/** UnderLinedスタイル Smallサイズ */
///	Font FONT_UL_SMALL  = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_SMALL);
///	/** UnderLinedスタイル Mediumサイズ */
///	Font FONT_UL_MEDIUM = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_MEDIUM);
///	/** UnderLinedスタイル Largeサイズ */
///	Font FONT_UL_LARGE  = Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_LARGE);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// @@@@ アンカー @@@@ //
	
	/** アンカー 左上 */
	int ANCHOR_LT = Graphics.TOP|Graphics.LEFT;
	/** アンカー 右上 */
	int ANCHOR_RT = Graphics.TOP|Graphics.RIGHT;
	/** アンカー 中央上 */
	int ANCHOR_CT = Graphics.TOP|Graphics.HCENTER;

///	/** アンカー 左下 */
///	int ANCHOR_LB = Graphics.BOTTOM|Graphics.LEFT;
///	/** アンカー 右下 */
///	int ANCHOR_RB = Graphics.BOTTOM|Graphics.RIGHT;
///	/** アンカー 中央下 */
///	int ANCHOR_CB = Graphics.BOTTOM|Graphics.HCENTER;

///	/** アンカー 左中央 */
///	int ANCHOR_LC = Graphics.VCENTER|Graphics.LEFT;
///	/** アンカー 右中央 */
///	int ANCHOR_RC = Graphics.VCENTER|Graphics.RIGHT;
///	/** アンカー 中央中央 */
///	int ANCHOR_CC = Graphics.VCENTER|Graphics.HCENTER;

///	/** アンカー 左ベースライン */
///	int ANCHOR_LBL = Graphics.BASELINE|Graphics.LEFT;
///	/** アンカー 右ベースライン */
///	int ANCHOR_RBL = Graphics.BASELINE|Graphics.RIGHT;
///	/** アンカー 中央ベースライン */
///	int ANCHOR_CBL = Graphics.BASELINE|Graphics.HECENTER;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// @@@@ 色 @@@@ //

	/** マゼンタ (紅紫色)             */	int  COLOR_MAGENTA         = 0x00FF00FF; //★
	/** シアン   (空色)               */	int  COLOR_CYAN            = 0x0000FFFF; //★
///	/** 桜色                          */	int  COLOR_SAKURA          = 0x00FEEEED;
///	/** オレンジ (橙色)               */	int  COLOR_ORANGE          = 0x00F39800;
///	/** スノーホワイト                */	int  COLOR_SNOWWHITE       = 0x00F4FBFE;
///	/** 砂色                          */	int  COLOR_SAND            = 0x00D3C6A6;
///	/** 土色                          */	int  COLOR_SOIL            = 0x00B47142;
///	/** セピア                        */	int  COLOR_SEPIA           = 0x006B4A2B;
	/** ダークグレイ                  */	int  COLOR_DARKGRAY        = 0x00404040;
	/** ダークシアン                  */	int  COLOR_DARKCYAN        = 0x0000C0C0;


	// @@@@ 基本１６色(webcolor?) @@@@ //

	/** レッド   (赤色)               */	int  COLOR_RED             = 0x00FF0000; //★
///	/** マルーン (栗色)               */	int  COLOR_MAROON          = 0x00800000;
///	/** フクシャ (赤紫色)             */	int  COLOR_FUCHSIA         = COLOR_MAGENTA;
///	/** パープル (紫色)               */	int  COLOR_PURPLE          = 0x00800080;
///	/** ライム   (ライム色)           */	int  COLOR_LIME            = 0x0000FF00; //★
///	/** グリーン (緑色)               */	int  COLOR_GREEN           = 0x00008000;
	/** イエロー (黄色)               */	int  COLOR_YELLOW          = 0x00FFFF00; //★
///	/** オリーブ (オリーブ色)         */	int  COLOR_OLIVE           = 0x00808000;
///	/** ブルー   (青色)               */	int  COLOR_BLUE            = 0x000000FF; //★
///	/** ネイビー (紺色)               */	int  COLOR_NAVY            = 0x00000080;
///	/** アクア   (水色)               */	int  COLOR_AQUA            = COLOR_CYAN;
///	/** ティール (青緑色)             */	int  COLOR_TEAL            = 0x00008080;
	/** ホワイト (白色)               */	int  COLOR_WHITE           = 0x00FFFFFF; //★
	/** シルバー (明るい灰色)         */	int  COLOR_SILVER          = 0x00C0C0C0; //★
	/** グレー   (灰色)               */	int  COLOR_GRAY            = 0x00808080; //★
	/** ブラック (黒色)               */	int  COLOR_BLACK           = 0x00000000; //★

	// @@@@ その他(webcolor?) @@@@ //

///	/** ピンク   (桃色)               */	int  COLOR_PINK            = 0x00FFC0CB;
///	/** ブラウン (茶色)               */	int  COLOR_BROWN           = 0x00A52A2A;
///	/** ゴールド (金色)               */	int  COLOR_GOLD            = 0x00FFD700;


	// @@@@ 赤系 @@@@ ///
///	/** ダークレッド                  */	int  COLOR_DARKRED         = 0x008B0000;
///	/** シナバー                      */	int  COLOR_CINNABAR        = 0x00E15A28;

	// @@@@ 青系 @@@@ //
///	/** ライトブルー                  */	int  COLOR_LIGHTBLUE       = 0x00ADD8E6;
///	/** ミディアムブルー              */	int  COLOR_MEDIUMBLUE      = 0x000000CD;
///	/** ダークブルー                  */	int  COLOR_DARKBLUE        = 0x0000008B;
///	/** アクアマリン                  */	int  COLOR_AQUAMARINE      = 0x007FFFD4;
///	/** アクアブルー                  */	int  COLOR_AQUABLUE        = 0x0000BFFF; //★
///	/** ライトスカイブルー            */	int  COLOR_LIGHTSKYBLUE    = 0x0087CEFA;
///	/** スカイブルー                  */	int  COLOR_SKYBLUE         = 0x0087CEEB;
///	/** ディープスカイブルー          */	int  COLOR_DEEPSKYBLUE     = 0x0000BFFF;
///	/** ミッドナイトブルー            */	int  COLOR_MIDNIGHTBLUE    = 0x00191970;
///	/** インディゴ                    */	int  COLOR_INDIGO          = 0x004B0082;
///	/** ターコイズブルー              */	int  COLOR_TURQUOISE       = 0x0040E0D0;

	// @@@@ 紫系 @@@@ //
///	/** ミディアムパープル            */	int  COLOR_MEDIUMPURPLE    = 0x009370DB;
///	/** バイオレット                  */	int  COLOR_VIOLET          = 0x00EE82EE;
///	/** ブルーバイオレット            */	int  COLOR_BLUEVIOLET      = 0x008A2BA2;
///	/** ダークバイオレット            */	int  COLOR_DARKVIOLET      = 0x009400D3;
///	/** ミディアムバイオレットレッド  */	int  COLOR_MEDIUMVIOLETRED = 0x00C71585;
///	/** パレオバイオレットレッド      */	int  COLOR_PALEOVIOLETRED  = 0x00DB7093;
///	/** ラベンダー                    */	int  COLOR_LAVENDER        = 0x00DFA0D2;
///	/** ラベンダーブラッシュ          */	int  COLOR_LAVENDERBLUSH   = 0x00FFF0F5;
	
	// @@@@ 白系 @@@@ //
///	/** アイボリー                    */	int  COLOR_IVORY           = 0x00FFFFF0;
///	/** フレッシュ (肌色)             */	int  COLOR_COLOR_FLESH     = 0x00FFE6CE;


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //DrawConstantsの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
