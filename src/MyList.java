// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyList.java  クラス  version 1.0
//
// (C)Tomarky   201*/01/01 - 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オリジナルクラス
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class MyList extends List implements CommandListener {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ フィールド(定数) @@@@//
	private static final String RS_DICE="tm.dice.dice";

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ フィールド(変数) @@@@ //

	private Command     cmdCancel=null;
	private Command     cmdLoad=null;
	private Command     cmdDelete=null;

	private DataSetter  dSetter=null;

	private Hashtable   dicelist=null;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/** 
	 * コンストラクタ
	 */
	public MyList(DataSetter ds) {
		super("ロード",Choice.IMPLICIT);
		dSetter=ds;

		cmdCancel=new Command("ｷｬﾝｾﾙ",Command.SCREEN,0);
		addCommand(cmdCancel);

		cmdLoad=new Command("ロード",Command.ITEM,0);
		setSelectCommand(cmdLoad);
		cmdDelete=new Command("削除",Command.ITEM,1);
		addCommand(cmdDelete);
		
		setCommandListener(this);
		
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void reset() {
		
		this.deleteAll();
		
		append("おみくじ(サンプル)",null);
		
		RecordStore       rs=null;
		RecordEnumeration re=null;
		
		if ((rs=open())==null) {
			return;
		}
		
		try {
			int c=rs.getNumRecords();
			byte[] b=null;
			String name=null;
			int n, k;
			
			if (c>0) {
				if (dicelist==null) {
					dicelist=new Hashtable();
				} else {
					dicelist.clear();
				}
			
				re=rs.enumerateRecords(null,null,false);
				while (re.hasNextElement()) {
					n=re.nextRecordId();
					b=rs.getRecord(n);
					name=getName(b);
					if (name!=null) {
						k=append(name,null);
					} else {
						k=append("<ERROR>",null);
					}
					dicelist.put(new Integer(k),new Integer(n));
				}
				
			}
			
		} catch (Exception e) {
			Debug.mes(e.toString());
			if (dicelist!=null) {
				dicelist.clear();
			}
			this.deleteAll();
		} finally {
			if (re!=null) {
				try {
					re.destroy();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					re=null;
				}
			}
			rs=close(rs);
		}
		
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private String getName(byte[] b) {
		ByteArrayInputStream  bais=null;
		DataInputStream       dis=null;
		String                name=null;
		
		try {
			bais=new ByteArrayInputStream(b);
			dis=new DataInputStream(bais);
			int n=0;
			n=(int)dis.readByte();
			name=new String(b,1,n);
			
		} catch (Exception e) {
			Debug.mes(e.toString());
		} finally {
			if (dis!=null) {
				try {
					dis.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					dis=null;
				}
			}
			if (bais!=null) {
				try {
					bais.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					bais=null;
				}
			}
		}
		return name;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * コマンドイベント
	 *
	 * @param c    アクションが発生したCommand
	 * @param disp cが配置されているDisplayable
	 */
	public void commandAction(Command c, Displayable disp) {
		if (c==cmdCancel) {
			dSetter.setData(null,0,0,0,0,null,null);
		} else if (c==cmdLoad) {
			int n=this.getSelectedIndex();
			if (n<0) {
				return;
			}
			if (getString(n).equals("おみくじ(サンプル)")) {
				String[] slist={"大吉","吉","中吉","小吉","半吉",
				          "末吉","末小吉","凶","小凶","半凶","末凶","大凶"};
				dSetter.setData("おみくじ",3,0,0,0,null,slist);
				return;
			}
			
			this.load(n);
			
		} else if (c==cmdDelete) {
			int n=this.getSelectedIndex();
			if (n<0) {
				return;
			}
			if (getString(n).equals("おみくじ(サンプル)")) {
				return;
			}
			
			RecordStore rs=null;
			if ((rs=open())==null) {
				return;
			}
			try {
				int id=((Integer)dicelist.get(new Integer(n))).intValue();
				rs.deleteRecord(id);
			} catch (Exception e) {
				Debug.mes(e.toString());
			} finally {
				rs=close(rs);
			}
			reset();
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void load(int p) {
		RecordStore rs=null;
		
		if ((rs=open())==null) {;
			return;
		}
		
		ByteArrayInputStream bais=null;
		DataInputStream      dis=null;
		
		try {
			byte[] data=null;
			
			int id=((Integer)dicelist.get(new Integer(p))).intValue();
			
			data=rs.getRecord(id);
			rs=close(rs);
			
			bais=new ByteArrayInputStream(data);
			dis=new DataInputStream(bais);
			
			int k=0,t=0,n1=0,n2=0,n3=0,z=0;
			int[] ilist=null;
			byte[] b=null;
			String[] slist=null;
			String name=null;

			k=(int)dis.readByte();
			b=new byte[k];
			dis.read(b,0,k);
			if (b!=null) {
				name=new String(b);
			}
			t=(int)dis.readByte();

			switch (t) {
			case 0:
				n1=dis.readInt();
				n2=dis.readInt();
				break;
			case 1:
				n1=dis.readInt();
				n2=dis.readInt();
				n3=dis.readInt();
				break;
			case 2:
				k=dis.readShort();
				ilist=new int[k];
				for (int i=0;i<k;i++) {
					ilist[i]=dis.readInt();
				}
				break;
			case 3:
				k=dis.readShort();
				slist=new String[k];
				for (int i=0;i<k;i++) {
					z=(int)dis.readByte();
					b=null;
					b=new byte[z];
					dis.read(b,0,z);
					if (b!=null) {
						slist[i]=new String(b);
					} else {
						slist[i]=null;
					}
				}
				break;
			}

			dSetter.setData(name,t,n1,n2,n3,ilist,slist);
			
		} catch (Exception e) {
			Debug.mes(e.toString());
		} finally {
			if (dis!=null) {
				try {
					dis.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					dis=null;
				}
			}
			if (bais!=null) {
				try {
					bais.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					bais=null;
				}
			}
			rs=close(rs);
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private static RecordStore open() {
		RecordStore rs=null;
		
		try {
			rs=RecordStore.openRecordStore(RS_DICE,true);
		} catch (Exception e) {
			Debug.mes(e.toString());
			if (rs!=null) {
				rs=close(rs);
			}
		} finally {
			return rs;
		}
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private static RecordStore close(RecordStore rs) {
		if (rs==null) {
			return null;
		}
		try {
			rs.closeRecordStore();
		} catch (Exception e) {
			Debug.mes(e.toString());
		} finally {
			return null;
		}
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public static boolean save(String name, int type, int n1, int n2, int n3,
	                                               int[] ilist, String[] slist) {
		
		if (  (name==null)
		        ||(type<0)
		        ||(type>3) 
		        ||((type==2)&&(ilist==null))
		        ||((type==3)&&(slist==null)) ) {
			return false;
		}
		
		ByteArrayOutputStream baos=null;
		DataOutputStream      dos=null;
		RecordStore           rs=null;
		boolean               result=true;
		
		
		try {
			baos=new ByteArrayOutputStream(12);
			dos=new DataOutputStream(baos);
			
			byte[] b=null;
			
			b=name.getBytes();
			
			dos.writeByte(b.length);
			dos.write(b,0,b.length);
			dos.writeByte(type);
			
			switch (type) {
			case 0:
				dos.writeInt(n1);
				dos.writeInt(n2);
				break;
			case 1:
				dos.writeInt(n1);
				dos.writeInt(n2);
				dos.writeInt(n3);
				break;
			case 2:
				dos.writeShort(ilist.length);
				for (int i=0;i<ilist.length;i++) {
					dos.writeInt(ilist[i]);
				}
				break;
			case 3:
				dos.writeShort(slist.length);
				for (int i=0;i<slist.length;i++) {
					b=slist[i].getBytes();
					dos.writeByte(b.length);
					dos.write(b,0,b.length);
				}
				break;
			}
			
			b=baos.toByteArray();
			
			rs=open();
			if ((rs!=null)&&(b!=null)) {
				rs.addRecord(b,0,b.length);
			} else {
				result=false;
			}
			
		} catch (Exception e) {
			Debug.mes(e.toString());
			result=false;
		} finally {
			if (dos!=null) {
				try {
					dos.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					dos=null;
				}
			}
			if (baos!=null) {
				try {
					baos.close();
				} catch (Exception e) {
					Debug.mes(e.toString());
				} finally {
					baos=null;
				}
			}
			rs=close(rs);
		}
		return result;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //MyListの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
