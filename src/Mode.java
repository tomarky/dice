// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Mode.java  インターフェース   version 1.0
//
// (C)Tomarky   201*/01/01 -
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インターフェースのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オリジナルインターフェース
 *
 * @author  Tomarky
 * @version 1.0
 */
public abstract class Mode implements OAPConstants, DrawConstants {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//

	public static final int NOCHANGE=0;
	public static final int NEXT=1;
	public static final int BACK=2;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	protected boolean           runningflag=true;
	protected boolean           stopall=true;
	protected boolean           autostop=true;
	protected boolean           rolling=false;
	protected boolean           animation=true;
	protected boolean           totalshown=false;

	protected boolean           stopper=false;

	protected MyRandom[]        randomlist=null;

	protected int               dice_decision=0;
	protected int               dice_count=0;
	protected int               stoptime=0;
	protected int               dice_total;
	protected int[]             dice_number=null;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public abstract String name();
	public abstract int keyMenu(int keyEvent);
//	public abstract boolean keyPressed(int keyEvent);
//	public abstract void stop();
//	public abstract void start(Refresher rf);
//	public abstract void paint(Graphics g);
	public abstract MyMenu getMenu(boolean modechange);

	public boolean keyPressed(int keyEvent) {
		if (stopper) {
			if (keyEvent==KEY_CLEAR) {
				stopper=false;
			}
		} else if (rolling) {
			if (keyEvent==Canvas.FIRE) {
				if (autostop==false) {
					if (stopall) {
						dice_decision=dice_count;
					} else {
						dice_decision++;
					}
				}
			}
		} else {
			switch (keyEvent) {
			case Canvas.FIRE:
				if (animation) {
					rolling=true;
					if (autostop) {
						if (stopall) {
							stoptime=30;
						} else {
							stoptime=30-dice_count;
						}
					}
					return true;
				} else {
					setDiceNewNumber();
					calcDiceTotal();
				}
				break;
			}
		}
		return false;
	}
	
	public void paint(Graphics g) {
		g.setFont(FONT_SMALL);
		g.setColor(COLOR_WHITE);
		if (stopper) {
			g.drawString("Push CLEAR to Ready",DISPLAY_WIDTH/2,200,ANCHOR_CT);
		} else if (!rolling) {
			g.drawString("Push SELECT"+(animation?" to Start":""),DISPLAY_WIDTH/2,200,ANCHOR_CT);
		} else {
			if (!autostop) {
				g.drawString("Push SELECT to Stop",DISPLAY_WIDTH/2,200,ANCHOR_CT);
			}
		}

		if (totalshown) {
			g.setFont(FONT_LARGE);
			//g.setColor(COLOR_WHITE);
			g.drawString("Total ... "+Integer.toString(dice_total),DISPLAY_WIDTH/2,DISPLAY_HEIGHT-25,
			                        ANCHOR_CT);
		}
	}

	public void stop() {
		runningflag=false;
	}

	public void start(Refresher rf) {
		int count=0;
		MyWait mywait=new MyWait();
		while ((dice_decision<dice_count)&&(runningflag)) {
			if (autostop) {
				count++;
				if (count>=stoptime) {
					count=0;
					if (stopall) {
						dice_decision=dice_count;
					} else {
						dice_decision++;
					}
				}
			}
			setDiceNewNumber();
			calcDiceTotal();
			rf.refresh();
			mywait.await(80L);
		}
		rolling=false;
		dice_decision=0;
		mywait=null;
		if (!autostop) {
			stopper=true;
		}
	}

	/**
	 *
	 */
	protected void setDiceNewNumber() {
		for (int i=dice_decision;i<dice_count;i++) {
			dice_number[i]=randomlist[i].getNext();
		}
	}

	/**
	 *
	 */
	protected void calcDiceTotal() {
		dice_total=0;
		for (int i=0;i<dice_count;i++) {
			dice_total+=dice_number[i];
		}
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //Modeの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
