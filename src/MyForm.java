// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyForm.java  フォーム  version 1.0
//
// (C)Tomarky   201*/01/01 -
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オリジナルFormクラス
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class MyForm extends Form implements CommandListener,
                                                  ItemStateListener{


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	private ChoiceGroup   cgType=null;
	private StringItem    siExplain=null;
	private TextField     tfName=null;
	private TextField     tfNum1=null;
	private TextField     tfNum2=null;
	private TextField     tfNum3=null;
	private StringItem    siDiceList=null;
	
	private Command       cmdAdd=null;
	private Command       cmdBack=null;
	private Command       cmdEdit=null;
	
	private List          list=null;
	private Command       cmdListAdd=null;
	private Command       cmdListDel=null;
	private Command       cmdListBack=null;

	private TextBox       tbInput=null;
	private Command       cmdInpAdd=null;
	private Command       cmdInpBack=null;

	private DataSetter    dtSetter=null;
	
	private MIDlet        midlet=null;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/**
	 * コンストラクタ
	 */
	public MyForm(DataSetter ds, MIDlet m) {
		super("サイコロ追加");
		//form=new Form("サイコロ追加");
		cgType=new ChoiceGroup("タイプ",Choice.POPUP);
		cgType.append("最大最小",null);
		cgType.append("増減",null);
		cgType.append("全指定(整数)",null);
		cgType.append("全指定(文字)",null);
		append(cgType);
		append(siExplain=new StringItem(null,"最小値〜最大値までの整数のサイコロです\n"));
		append(new Spacer(240,10));
		tfName=new TextField("サイコロ名\n","サイコロ",8,TextField.ANY);
		tfNum1=new TextField("最小値\n","1",7,TextField.NUMERIC);
		tfNum2=new TextField("最大値\n","6",7,TextField.NUMERIC);
		append(tfName);
		append(new Spacer(240,10));
		append(tfNum1);
		append(new Spacer(240,10));
		append(tfNum2);
		addCommand(cmdBack=new Command("ｷｬﾝｾﾙ",Command.SCREEN,0));
		addCommand(cmdAdd=new Command("ＯＫ",Command.SCREEN,1));
		setCommandListener(this);
		setItemStateListener(this);
		
		dtSetter=ds;
		midlet=m;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void reset() {
		this.setTitle("サイコロ追加");
		tfName.setString("サイコロ");
		cgType.setSelectedIndex(0,true);
		itemStateChanged(cgType);
		if (list!=null) {
			list.deleteAll();
			System.gc();
		}
	}
	
	public void reset(String name, int t, int n1, int n2, int n3, int[] ilist, String[] slist) {
		this.setTitle("サイコロ変更");
		tfName.setString(name);
		cgType.setSelectedIndex(t,true);
		itemStateChanged(cgType);
		if (list!=null) {
			list.deleteAll();
			System.gc();
		}
		tfNum1.setString(Integer.toString(n1));
		if (t==0) {
			tfNum2.setMaxSize(7);
		} else if (t==1) {
			tfNum2.setMaxSize(4);
		}
		tfNum2.setString(Integer.toString(n2));
		if (tfNum3==null) {
			tfNum3=new TextField("回数\n","5",4,TextField.NUMERIC);
		}
		tfNum3.setString(Integer.toString(n3));
		if (t==2) {
			if (list==null) {
				makeList();
			}
			for (int i=0;i<ilist.length;i++) {
				list.append(Integer.toString(ilist[i]),null);
			}
			makeValueList();
		} else if (t==3) {
			if (list==null) {
				makeList();
			}
			for (int i=0;i<slist.length;i++) {
				list.append(slist[i],null);
			}
			makeValueList();
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void makeList() {
		list=new List("値リスト編集",Choice.IMPLICIT);
		list.addCommand(cmdListBack=new Command("戻る",Command.SCREEN,0));
		list.addCommand(cmdListAdd=new Command("追加",Command.SCREEN,1));
		list.addCommand(cmdListDel=new Command("削除",Command.SCREEN,2));
		list.setCommandListener(this);
	}
	
	private void makeValueList() {
		if (siDiceList==null) {
			siDiceList=new StringItem(null,"[値リスト]\nなし");
		} else {
			siDiceList.setText("[値リスト]\nなし");
		} 
		if (list==null) {
			return; 
		}
		if (list.size()>0) {
			String s="[値リスト]\n";
			for (int i=0;i<list.size();i++) {
				s+=list.getString(i)+"\n";
			}
			siDiceList.setText(s);
		} 
	}

	/**
	 * コマンドイベント
	 *
	 * @param c    アクションが発生したCommand
	 * @param disp cが配置されているDisplayable
	 */
	public void commandAction(Command c,Displayable disp) {
		if (c==cmdBack) {
			dtSetter.setData(null,-1,0,0,0,null,null);
		} else if (c==cmdEdit) {
			if (list==null) {
				makeList();
			}
			(Display.getDisplay(midlet)).setCurrent(list);
		} else if (c==cmdAdd) {
			if ((tfName.getString()).length()==0) {
				return;
			}
			int t1,t2,t3;
			switch (cgType.getSelectedIndex()) {
			case 0:
				if 	(	   ((tfNum1.getString()).length()==0) 
						|| ((tfNum2.getString()).length()==0) ) {
					return;
				}
				t1=Integer.parseInt(tfNum1.getString());
				t2=Integer.parseInt(tfNum2.getString());
				dtSetter.setData(tfName.getString(),0,t1,t2,0,null,null);
				break;
			case 1:
				if 	(	   ((tfNum1.getString()).length()==0) 
						|| ((tfNum2.getString()).length()==0) 
						|| ((tfNum3.getString()).length()==0) ) {
					return;
				}
				t1=Integer.parseInt(tfNum1.getString());
				t2=Integer.parseInt(tfNum2.getString());
				t3=Integer.parseInt(tfNum3.getString());
				dtSetter.setData(tfName.getString(),1,t1,t2,t3,null,null);
				break;
			case 2:
				if (list==null) {
					return;
				}
				if (list.size()==0) {
					return;
				}
				int[] ilist=new int[list.size()];
				for (int i=0;i<list.size();i++) {
					ilist[i]=Integer.parseInt(list.getString(i));
				}
				dtSetter.setData(tfName.getString(),2,0,0,0,ilist,null);
				break;
			case 3:
				if (list==null) {
					return;
				}
				if (list.size()==0) {
					return;
				}
				String[] slist=new String[list.size()];
				for (int i=0;i<list.size();i++) {
					slist[i]=list.getString(i);
				}
				dtSetter.setData(tfName.getString(),3,0,0,0,null,slist);
				break;
			}
		} else if (c==cmdListBack) {
			makeValueList();
			(Display.getDisplay(midlet)).setCurrent(this);
		} else if (c==cmdListAdd) {
			if (cmdInpBack==null) {
				cmdInpBack=new Command("ｷｬﾝｾﾙ",Command.SCREEN,0);
			}
			if (cmdInpAdd==null) {
				cmdInpAdd=new Command("ＯＫ",Command.SCREEN,1);
			}
			if (cgType.getSelectedIndex()==2) {
				tbInput=new TextBox("値(整数)","0",7,TextField.NUMERIC);
			} else {
				tbInput=new TextBox("値(文字)","？",10,TextField.ANY);
			}
			tbInput.addCommand(cmdInpBack);
			tbInput.addCommand(cmdInpAdd);
			tbInput.setCommandListener(this);
			(Display.getDisplay(midlet)).setCurrent(tbInput);
		} else if (c==cmdListDel) {
			int n=list.getSelectedIndex();
			if (n>=0) {
				list.delete(n);
			}
		
		} else if (c==cmdInpBack) {
			(Display.getDisplay(midlet)).setCurrent(list);
			tbInput.removeCommand(cmdInpAdd);
			tbInput.removeCommand(cmdInpBack);
			tbInput.setCommandListener(null);
			tbInput=null;
			System.gc();
		} else if (c==cmdInpAdd) {
			String s=tbInput.getString();
			if (s.length()==0) {
				return;
			}
			list.append(s.replace('\n',' '),null);
			(Display.getDisplay(midlet)).setCurrent(list);
			tbInput.removeCommand(cmdInpAdd);
			tbInput.removeCommand(cmdInpBack);
			tbInput.setCommandListener(null);
			tbInput=null;
			System.gc();
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	/**
	 * アイテムステートチェンジイベント
	 *
	 * @param item ステートチェンジしたアイテム
	 */
	public void itemStateChanged(Item item) {
		if (item.equals(cgType)) {
			int n=size();
			for (int i=0;i<n-5;i++) {
				delete(5);
			}
			if (cmdEdit!=null) {
				removeCommand(cmdEdit);
			}
			switch (cgType.getSelectedIndex()) {
			case 0:
				siExplain.setText("最小値〜最大値までの整数のサイコロです\n");
				tfNum1.setLabel("最小値\n");
				tfNum1.setString("1");
				tfNum2.setLabel("最大値\n");
				tfNum2.setString("6");
				tfNum2.setMaxSize(7);
				append(tfNum1);
				append(new Spacer(240,10));
				append(tfNum2);
				break;
			case 1:
				siExplain.setText("初期値から増減量分ずつ値が変化する整数のサイコロです\n");
				tfNum1.setLabel("初期値\n");
				tfNum1.setString("1");
				tfNum2.setLabel("増減量\n");
				tfNum2.setString("1");
				tfNum2.setMaxSize(4);
				if (tfNum3==null) {
					tfNum3=new TextField("回数\n","5",4,TextField.NUMERIC);
				} else {
					tfNum3.setString("5");
				}
				append(tfNum1);
				append(new Spacer(240,10));
				append(tfNum2);
				append(new Spacer(240,10));
				append(tfNum3);
				break;
			case 2:
				siExplain.setText("出目(整数)の全てを個別に設定できるサイコロです。"
				                  +"メニューから「編集」を選らんでください\n");
				if (siDiceList==null) {
					siDiceList=new StringItem(null,"[値リスト]\nなし");
				} else {
					siDiceList.setText("[値リスト]\nなし");
				}
				if (list!=null) {
					if (list.size()>0) {
						list.deleteAll();
						System.gc();
					}
				}
				append(siDiceList);
				if (cmdEdit==null) {
					cmdEdit=new Command("編集",Command.SCREEN,2);
				}
				addCommand(cmdEdit);
				break;
			case 3:
				siExplain.setText("出目(文字)の全てを個別に設定できるサイコロです。"
				                  +"メニューから「編集」を選らんでください\n");
				if (siDiceList==null) {
					siDiceList=new StringItem(null,"[値リスト]\nなし");
				} else {
					siDiceList.setText("[値リスト]\nなし");
				}
				if (list!=null) {
					if (list.size()>0) {
						list.deleteAll();
						System.gc();
					}
				}
				append(siDiceList);
				if (cmdEdit==null) {
					cmdEdit=new Command("編集",Command.SCREEN,2);
				}
				addCommand(cmdEdit);
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //MyFormの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
