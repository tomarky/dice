// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyDice.java  クラス      version 1.0
//
// (C)Tomarky   2010/04/04 - 2010/04/05
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage; //パッケージの指定

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //乱数や時間
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * <B>サイコロのクラス</B>
 * @author  Tomarky
 * @version 1.0
 */
public final class MyDice implements DrawConstants {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	/** スプライト */
	private Sprite sp=null;


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/**
	 * 一般的な配色のサイコロ
	 *
	 * @param newsize サイコロのサイズ
	 */
	public MyDice(int newsize) {
		setDice(newsize,COLOR_WHITE,
		                COLOR_BLACK,
		                COLOR_RED,
		                COLOR_BLACK);
	}

	/**
	 * 配色を指定したのサイコロ
	 *
	 * @param newsize サイコロのサイズ 10以上40以下
	 * @param backcolor  サイコロの背景色 0x00RRGGBB の形式で渡します
	 * @param linecolor  サイコロのふちの色 0x00RRGGBB の形式で渡します
	 * @param starcolor  １の目の色 0x00RRGGBB の形式で渡します
	 * @param starscolor １の目以外の目の色 0x00RRGGBB の形式で渡します
	 */
	public MyDice(int newsize,
	              int backcolor,
	              int linecolor,
	              int starcolor,
	              int starscolor) {
		setDice(newsize,backcolor,linecolor,starcolor,starscolor);
	}


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	/**
	 * サイコロ画像の生成する
	 *
	 * @param newsize    サイコロのサイズ 10以上40以下
	 * @param backcolor  サイコロの背景色 0x00RRGGBB の形式で渡します
	 * @param linecolor  サイコロのふちの色 0x00RRGGBB の形式で渡します
	 * @param starcolor  １の目の色 0x00RRGGBB の形式で渡します
	 * @param starscolor １の目以外の目の色 0x00RRGGBB の形式で渡します
	 */
	private void setDice(int newsize,
	                     int backcolor,
	                     int linecolor, 
	                     int starcolor,
	                     int starscolor) {
		//使用するサイズ
		int size=newsize<10?10:(newsize>40?40:newsize);
		//イメージの生成
		Image img=Image.createImage(size*6,size);
		//描写機能の取得
		Graphics g=img.getGraphics();
		
		//背景
		g.setColor(backcolor);
		g.fillRect(0,0,img.getWidth(),img.getHeight());

		//サイコロの枠
		g.setColor(linecolor);
		for (int i=0;i<6;i++) {
			g.drawRect(i*size,0,size-1,size-1);
		}

		//目の直径, オフセットX座標
		int r,dx;

		//１の目
		g.setColor(starcolor);
		r=size/2;
		g.fillArc((size-r)/2,(size-r)/2,r,r,0,360);
		
		//１の目以外
		g.setColor(starscolor);
		r=size/4;

		int p12=(size-r)/2;     //サイコロの２分の１
		int p14=(size-r*2)/4;   //サイコロの４分の１
		int p34=(size*3-r*2)/4; //サイコロの４分の３
		
		//２の目
		dx=size;
		g.fillArc(dx+p34,p14,r,r,0,360);
		g.fillArc(dx+p14,p34,r,r,0,360);
		
		//３の目
		dx+=size;
		g.fillArc(dx+p14,p14,r,r,0,360);
		g.fillArc(dx+p12,p12,r,r,0,360);
		g.fillArc(dx+p34,p34,r,r,0,360);
		
		//４の目
		dx+=size;
		g.fillArc(dx+p14,p14,r,r,0,360);
		g.fillArc(dx+p14,p34,r,r,0,360);
		g.fillArc(dx+p34,p14,r,r,0,360);
		g.fillArc(dx+p34,p34,r,r,0,360);
		
		//５の目
		dx+=size;
		g.fillArc(dx+p14,p14,r,r,0,360);
		g.fillArc(dx+p14,p34,r,r,0,360);
		g.fillArc(dx+p34,p14,r,r,0,360);
		g.fillArc(dx+p34,p34,r,r,0,360);
		g.fillArc(dx+p12,p12,r,r,0,360);
		
		//６の目
		dx+=size;
		g.fillArc(dx+p14,p14,r,r,0,360);
		g.fillArc(dx+p14,p34,r,r,0,360);
		g.fillArc(dx+p34,p14,r,r,0,360);
		g.fillArc(dx+p34,p34,r,r,0,360);
		g.fillArc(dx+p14,p12,r,r,0,360);
		g.fillArc(dx+p34,p12,r,r,0,360);
		
		//スプライトへの設定
		sp=new Sprite(Image.createImage(img),size,size);

		g=null;
		img=null;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	/**
	 * 現在のサイコロの出目を返す
	 *
	 * @return サイコロの出目を返す（１〜６の値）
	 */
	public int getNumber() {
		return sp.getFrame()+1;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの出目を指定する
	 *
	 * @param number 出目の数字。１〜６の値を渡す
	 */
	public void setNumber(int number) {
		if ((1<=number)&&(number<=6)) {
			sp.setFrame(number-1);
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの出目をランダムに変える
	 *
	 * @return サイコロの出目を返す
	 */
	public int setNumber() {
		sp.setFrame(MyRandom.rand(6));
		return getNumber();
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロを描写する
	 *
	 * @param g サイコロを描写したいGraphicsを渡す
	 */
	public void paint(Graphics g) {
		sp.paint(g);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロのサイズの取得
	 *
	 * @return サイコロのサイズを返す
	 */
	public int getSize() {
		return sp.getHeight();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示位置のX座標を取得
	 *
	 * @return サイコロの表示位置のX座標
	 */
	public int getX() {
		return sp.getX();
	}
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示位置のY座標を取得
	 *
	 * @return サイコロの表示位置のY座標
	 */
	public int getY() {
		return sp.getY();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示位置を移動する
	 *
	 * @param dx X軸方向の移動量
	 * @param dy Y軸方向の移動量
	 */
	public void move(int dx,int dy) {
		sp.move(dx,dy);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示状態を返す
	 *
	 * @return trueなら表示。falseなら非表示。
	 */
	public boolean isVisible() {
		return sp.isVisible();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示位置を設定する
	 *
	 * @param x 表示位置のX座標 
	 * @param y 表示位置のY座標
	 */
	public void setPosition(int x,int y) {
		sp.setPosition(x,y);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * サイコロの表示状態の設定
	 *
	 * @param visible trueなら表示。falseなら非表示
	 */
	public void setVisible(boolean visible) {
		sp.setVisible(visible);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //MyDiceの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
