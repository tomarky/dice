// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// OAPConstants.java  OAPの定数   version 1.0
//
// (C)Tomarky   2010/04/08 -
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//import java.util.*;                        //乱数や時間
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
//import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インターフェースのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オープンアプリの定数リスト
 *
 * @author  Tomarky
 * @version 1.0
 */
public interface OAPConstants {

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//

	/** ディスプレイの横幅 */
	int         DISPLAY_WIDTH               = 240;
	/** ディスプレイの縦幅 */
	int         DISPLAY_HEIGHT              = 268;
//	/** ディスプレイのフルスクリーン時の縦幅 */
//	int         DISPLAY_FULLSCREEN_HEIGHT   = DISPLAY_HEIGHT + 28;
//	/** "OAP-SCREEN-SIZE:WQVGA"指定時のディスプレイ縦幅 (JavaVM ver1.1以降) */
//	int         DISPLAY_WQVGA_HEIGHT        = 376;

//	/** オフスクリーンイメージの最大横幅 */
//	int         OFFSCREEN_MAX_WIDTH           = DISPLAY_WIDTH + 32;
//	/** オフスクリーンイメージの最大縦幅 */
//	int         OFFSCREEN_MAX_HEIGHT          = DISPLAY_FULLSCREEN_HEIGHT + 32;
//	/** "OAP-SCREEN-SIZE:WQVGA"指定時のオフスクリーンイメージの最大縦幅  (JavaVM ver1.1以降) */
//	int         OFFSCREEN_WQVGA_MAX_HEIGHT    = DISPLAY_WQVGA_HEIGHT + 32;

	/** フォントサイズ (SMALL) */
	int         FONTSIZE_SMALL  = 12;
	/** フォントサイズ (MIDEUM) */
	int         FONTSIZE_MIDEUM = 16;
	/** フォントサイズ (LARGE) */
	int         FONTSIZE_LARGE  = 20;
	
//	/** レコードストア最大容量 */
//	int         RECORDSTORE_MAX_SIZE = 32768;

	/** クリア(CLEAR)キーのキーコード */
	int         KEY_CLEAR = -8;
	/** 通話(SEND)キーのキーコード */
	int         KEY_SEND  = -10;
	/** SOFT1キーのキーコード */
	int         KEY_SOFT1 = -6;
	/** SOFT2キーのキーコード */
	int         KEY_SOFT2 = -7;
	/** SOFT3キーのキーコード */
	int         KEY_SOFT3 = -20;
	/** SOFT4キーのキーコード */
	int         KEY_SOFT4 = -21;
	
///	/** システムプロパティ名 BGM設定の状態。"on"、もしくは"off" */
///	String      PROPERTY_SETTING_BGMUSIC   = "com.kddi.vm.setting.bgmusic";
///	/** システムプロパティ名 バックグラウンドメール受信設定の状態。"on"、もしくは"off" */
///	String      PROPERTY_SETTING_BGMAIL    = "com.kddi.vm.setting.bgmail";
///	/** システムプロパティ名  オープンアプリプレイヤーが利用している本日の通信量 (byte単位) */
///	String      PROPERTY_NETUSE_CURRENT    = "com.kddi.vm.netuse.current";
///	/** システムプロパティ名 オープンアプリプレイヤーが一日に利用できる通信量 (byte単位) */
///	String      PROPERTY_NETUSE_MAX        = "com.kddi.vm.netuse.max";
//	/** システムプロパティ名 オープンアプリプレイヤー起動時のパラメータ (JavaVM ver1.1以降) */
//	String      PROPERTY_MIDLET_ARGS       = "com.kddi.midlet.args";
//	/** システムプロパティ名 電界強度0 (=圏外),1,2,3,4 (=アンテナ3本) (JavaVM ver1.1以降) */
//	String      PROPERTY_DEV_INTENSITY     = "com.kddi.dev.intensity";
//	/** システムプロパティ名 電池残量0 (=空),1,2,3 (=満),4 (=充電中)  (JavaVM ver1.1以降) */
//	String      PROPERTY_DEV_POWERSUPPLY   = "com.kddi.dev.powersupply";
//	/** システムプロパティ名 メモリカード有無情報 0 (無),1 (有),2 (非搭載) (JavaVM ver1.1以降) */
//	String      PROPERTY_DEV_MEMCARDSTATE   = "com.kddi.dev.memcardstate";
//	/** システムプロパティ名 Jadファイル取得元URL (JavaVM ver1.1以降) */
//	String      PROPERTY_MIDLET_JADURL      = "com.kddi.midlet.jadurl";

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //OAPConstantsの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
