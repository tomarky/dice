// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部  version 1.0
//
// (C)Tomarky   2010.03.19 -
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
import java.util.*;                        //乱数や時間
//import java.io.*;                          //InputStream等データ操作
import javax.microedition.midlet.*;        //MIDlet
import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * メイン処理部
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class Canvas1 extends GameCanvas implements Runnable,
                                                         CommandListener, Refresher,
                                                         OAPConstants,
                                                         DrawConstants {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//

	/** ウェイト */
	private final long        SLEEPTIME = 100L;

	private final int         MODE_DICE  = 0; 
	
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	/** MIDlet操作用 */
	private MIDlet            midlet;
	/** オフスクリーン描写用 */
	private Graphics          g;

	/** ウェイト用クラス */
	private MyWait            mywait=new MyWait(SLEEPTIME);

	/** メインループ強制中断用 */
	private boolean           RunningFlag=true;
	/** callFinishが呼ばれたかどうか */
	private boolean           CallFlag=false;

	/** キーイベント用 */
	private int               keyEvent=-999;

	private Thread            thread=null;

	private int               mode=MODE_DICE;
//	private boolean           menushown=false;

	private MyMenu            currentmenu=null;
	private Hashtable         commandlist=new Hashtable();

	private Mode              modemode=null;
	
	private Vector            modelist=new Vector(3);

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/**
	 * コンストラクタ
	 *
	 * @param m MIDlet
	 */
	public Canvas1(MIDlet m) {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		midlet=m;

		//コマンドの生成
		commandlist.put("終了",new Command("終了",  Command.SCREEN,0));
		commandlist.put("ﾒﾆｭｰ",new Command("ﾒﾆｭｰ",  Command.SCREEN,1));

		addCommand((Command)commandlist.get("終了"));
		addCommand((Command)commandlist.get("ﾒﾆｭｰ"));

		//コマンドリスナーの指定
		setCommandListener(this);

		//グラフィックスの取得
		g=getGraphics();
		
		modelist.addElement(new DiceMode());
		modelist.addElement(new SpDiceMode(midlet,this));

		modemode=(Mode)(modelist.elementAt(mode));
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 終了の知らせ。<BR>
	 * MIDlet.destroyApp内などから呼び出して終了処理をする。
	 */
	public void callFinish() {
		RunningFlag=false;
		CallFlag=true;
		modemode.stop();
		if (thread!=null) {
			try { thread.join(); } catch(Exception e) { Debug.mes(e.toString()); }
			thread=null;
		}
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public synchronized void refresh() {
		int i,x,y;

		g.setColor(COLOR_DARKGRAY);
		g.fillRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
		
		g.setFont(FONT_SMALL);
		g.setColor(COLOR_WHITE);
		g.drawString(modemode.name(),0,0,ANCHOR_LT);
		
		modemode.paint(g);
		
		if (hasMenuShown()) {
			currentmenu.paint(g);
		}
		
		flushGraphics();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * 実行（メインループ処理）
	 */
	public void run() {
		Debug.mes("Start");
		modemode.start(this);
		refresh();
		Debug.mes("End");
		thread=null;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * キープレスイベント
	 *
	 * @param keyCode キーコード
	 */
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		switch (keyCode) {
		case KEY_CLEAR:
		case KEY_SEND:
			keyEvent=keyCode;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
		
		int n;
		if (hasMenuShown()) {
			switch (keyEvent) {
			case UP:
				currentmenu.up();
				break;
			case DOWN:
				currentmenu.down();
				break;
			case LEFT:
			case RIGHT:
			case FIRE:
				n=modemode.keyMenu(keyEvent);
				switch (n) {
				case Mode.NEXT:
					mode=(mode+1)%modelist.size();
					modemode=(Mode)(modelist.elementAt(mode));
					break;
				case Mode.BACK:
					mode=(mode+modelist.size()-1)%modelist.size();
					modemode=(Mode)(modelist.elementAt(mode));
					break;
				}
				currentmenu=modemode.getMenu(n!=Mode.NOCHANGE);
				System.gc();
				//if (currentmenu==null) {
				//	menushown=false;
				//}
				break;
			case KEY_CLEAR:
				currentmenu=null;
				//menushown=false;
				break;
			}
			refresh();
			return;
		}
		
		if (modemode.keyPressed(keyEvent)) {
			System.gc();
			(thread=new Thread(this)).start();
		}
		refresh();
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	/**
	 * キーリピートイベント
	 *
	 * @param keyCode キーコード
	 * /
	public void keyRepeated(int keyCode) {
	}
//*/

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	/**
	 * キーリリースイベント
	 *
	 * @param keyCode キーコード
	 * /
	public void keyReleased(int keyCode) {
	}
//*/

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 * コマンドイベント
	 *
	 * @param c    アクションが発生したCommand
	 * @param disp cが配置されているDisplayable
	 */
	public void commandAction(Command c,Displayable disp) {
		if (c.equals(commandlist.get("終了"))) {
			midlet.notifyDestroyed();
		} else if (c.equals(commandlist.get("ﾒﾆｭｰ"))) {
			Debug.mes("メニューが押されました");
			
			if (hasMenuShown()) {
				currentmenu=null;
				//menushown=false;
				refresh();
				return;
			}

			currentmenu=modemode.getMenu(false);
			System.gc();
			//if (currentmenu!=null) {
			//	menushown=true;
			//}
			refresh();
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean hasMenuShown() {
		return currentmenu!=null;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //Canvas1の終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
