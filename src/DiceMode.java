// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// DiceMode.java  クラス  version 1.0
//
// (C)Tomarky   201*/01/01 - 
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ 説明 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ パッケージの指定 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//package MyPackage;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ インポート @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
//import java.util.*;                        //Random,Calendar,Timer,Vector,Hashtableなど
//import java.io.*;                          //InputStream等データ操作
//import javax.microedition.midlet.*;        //MIDlet
//import javax.microedition.lcdui.game.*;    //GameCanvas,Layer,Sprite,TiledLayer
import javax.microedition.lcdui.*;         //Canvas,Graphics,Image,Form,Font,Display
//import javax.microedition.rms.*;           //レコードストア
//import javax.microedition.media.*;         //音楽ファイル再生 Manager,Player
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;              //HTTP通信
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ クラスのコード @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
/**
 * オリジナルクラス
 *
 * @author  Tomarky
 * @version 1.0
 */
public final class DiceMode extends Mode {


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 定数 @@@@//


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ 変数 @@@@ //

	private MyMenu            currentmenu=null;

	private MyDice            mydice=null;

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ コンストラクタ @@@@ //

	/** 
	 * コンストラクタ
	 */
	public DiceMode() {
		randomlist=new MyRandom[20];
		dice_number=new int[20];
		mydice=new MyDice(40);
		
		for(int i=0;i<20;i++) {
			randomlist[i]=new MyRandom(1,6);
		}
		
		dice_count=1;
		setDiceNewNumber();
		calcDiceTotal();
	}


	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@ メソッド @@@@ //

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public String name() {
		return "サイコロ";
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public int keyMenu(int keyEvent) {
		if (currentmenu==null) {
			return NOCHANGE;
		}
		int n=currentmenu.getSelect();
		switch (n) {
		case 0: 
			switch (keyEvent) {
			case Canvas.LEFT:
				if (dice_count>1) {
					dice_count--;
				}
				break;
			case Canvas.RIGHT:
				if (dice_count<20) {
					dice_count++;
				}
				break;
			case Canvas.FIRE:
				dice_count=(dice_count%20)+1;
				break;
			}
			break;
		case 1: 
			totalshown=!totalshown;
			break;
		case 2:	
			animation=!animation;
			break;
		case 3:
			if (animation) {
				autostop=!autostop;
			}
			break;
		case 4:
			if (animation) {
				stopall=!stopall;
			}
			break;
		case 5:
			switch (keyEvent) {
			case Canvas.LEFT:
				return BACK;
			case Canvas.RIGHT:
			case Canvas.FIRE:
				return NEXT;
			}
		}
		return NOCHANGE;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void paint(Graphics g) {
		int x0=(DISPLAY_WIDTH-45*5)/2, y0=20;
		int x,y;
		for (int i=0;i<dice_count;i++) {
			x=i%5; y=i/5;
			mydice.setPosition(getDiceX(i), getDiceY(i));
			mydice.setNumber(dice_number[i]);
			mydice.paint(g);
		}
			g.setFont(FONT_SMALL);
			g.setColor(COLOR_WHITE);
		if (rolling) {
			if ((dice_decision>=0)&&(dice_decision<dice_count)&&(!stopall)) {
				x=dice_decision%5;
				y=dice_decision/5;
				g.setColor(autostop?COLOR_RED:COLOR_CYAN);
				g.drawRect(getDiceX(dice_decision)-1,getDiceY(dice_decision)-1,41,41);
			}
		}
		super.paint(g);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public MyMenu getMenu(boolean modechange) {
		if (rolling) {
			return null;
		}
		MyMenu menu=new MyMenu(10);
		menu.setTitle("メニュー");
		menu.addMenu("サイコロの数 "+Integer.toString(dice_count));
		menu.addMenu("合計表示 "+(totalshown?"ON":"OFF"));
		menu.addMenu("ｱﾆﾒｰｼｮﾝ "+(animation?"ON":"OFF"));
		menu.addMenu("自動停止 "+(autostop?"ON":"OFF"));
		menu.addMenu("一斉停止 "+(stopall?"ON":"OFF"));
		menu.addMenu("モード変更");
		if (animation==false) {
			menu.setSelectable(false,3);
			menu.setSelectable(false,4);
		}
		if (currentmenu!=null) {
			int n=currentmenu.getSelect();
			menu.setSelect(n);
			currentmenu.removeMenuAll();
		}
		if (modechange) {
			menu.setSelect(5);
		}
		currentmenu=menu;
		menu=null;
		return currentmenu;
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 *
	 */
	private int getDiceX(int index) {
		int x0;
		int n1=(dice_count-1)/5;
		int n2=index/5;
		if ((dice_count%5==0)||(n1!=n2)) {
			x0=(DISPLAY_WIDTH-45*5)/2;
			return x0+(index%5)*45;
		}
		int n3=((dice_count-1)%5)+1;
		x0=(DISPLAY_WIDTH-40*n3)/(n3+1);
		return x0+(index%5)*(40+x0);
	}

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	/**
	 *
	 */
	private int getDiceY(int index) {
		if (dice_count>15) {
			return 20+(index/5)*45;
		}
		if (dice_count>10) {
			return 20+(index/5)*60;
		}
		if (dice_count>5) {
			return 20+(index/5)*80+20;
		}
		return 80;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
} //DiceModeの終わり
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
